package sample.platform_specific

expect class MutableObservable<T>() : Observable<T> {
    fun newValue(value: T) // used a different name from `postValue` due to Java-Kotlin visibility bug
}
