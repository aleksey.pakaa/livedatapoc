package sample.ui

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import sample.R
import sample.SampleViewModel
import sample.platform_specific.AndroidPlatformProvider
import sample.viewModels

class MainActivity : AppCompatActivity() {

    private val viewModel: SampleViewModel by viewModels { SampleViewModel(AndroidPlatformProvider()) }
    private lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView = findViewById(R.id.main_text)
        viewModel.observable.observe(this, ::render)
    }

    private fun render(text: String?) {
        textView.text = text
    }
}
