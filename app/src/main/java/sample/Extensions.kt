package sample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.viewModels
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle

inline fun <reified T : androidx.lifecycle.ViewModel> ComponentActivity.viewModels(
    noinline provider: ((SavedStateHandle) -> T)? = null
): Lazy<T> {
    return if (provider == null) {
        viewModels()
    } else {
        viewModels {
            object : AbstractSavedStateViewModelFactory(this, Bundle()) {
                @Suppress("UNCHECKED_CAST") // converting VM in the create will always cause an unchecked cast but it is safe
                override fun <VM : androidx.lifecycle.ViewModel?> create(
                    key: String,
                    modelClass: Class<VM>,
                    handle: SavedStateHandle
                ): VM =
                    provider(handle) as VM
            }
        }
    }
}
