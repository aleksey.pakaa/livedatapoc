package sample.platform_specific

class AndroidPlatformProvider : PlatformProvider {
    override val platformName: String = "Android"
}
