package sample.platform_specific

import androidx.lifecycle.LiveData

actual typealias Observable<T> = LiveData<T>
