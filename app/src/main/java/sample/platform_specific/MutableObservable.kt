package sample.platform_specific

import androidx.lifecycle.LiveData

actual class MutableObservable<T> : LiveData<T>() {
    actual fun newValue(value: T) {
        super.postValue(value)
    }
}
