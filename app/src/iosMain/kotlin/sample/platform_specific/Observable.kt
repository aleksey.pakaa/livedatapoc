package sample.platform_specific

actual abstract class Observable<T> {
    protected var currentValue: T? = null
    private val observers = mutableListOf<(T?) -> Unit>()

    fun observe(observer: (T?) -> Unit) {
        if (!observers.contains(observer)) {
            observers.add(observer)
            observer(currentValue)
        }
    }

    fun unsubscribe(observer: (T?) -> Unit) {
        observers.remove(observer)
    }

    actual fun getValue(): T? = currentValue
}
